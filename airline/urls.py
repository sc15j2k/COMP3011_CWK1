"""airline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^generatebasicsettings/',views.generateBasicSettings),
    url(r'^generate/', views.generateFlightSchedule),
    url(r'^greeting/', views.greeting),
    url(r'^api/test/', views.test),
    url(r'^api/findflight/', views.findFlight),
    url(r'^api/bookflight/', views.bookingFlight),
    url(r'^api/paymentmethods/', views.paymentMethods),
    url(r'^api/payforbooking/', views.payForBooking),
    url(r'^api/finalizebooking/',views.finalizeBooking),
    url(r'^api/bookingstatus/',views.bookingStatus),
    url(r'^api/cancelbooking/',views.cancelBooking)
]
