from django.contrib import admin
from .models import *

admin.site.register(Aircraft)
admin.site.register(Airport)
admin.site.register(Flights)
admin.site.register(Passenger)
admin.site.register(Bookings)
admin.site.register(Payment)
admin.site.register(Invoice)
